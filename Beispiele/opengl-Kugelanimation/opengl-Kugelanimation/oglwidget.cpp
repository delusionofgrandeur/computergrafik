#include "oglwidget.h"
#include "math.h"
OGLWidget::OGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    // Setup the animation timer to fire every x msec
    animtimer = new QTimer(this);
    animtimer->start( 50 );

    // Everytime the timer fires, the animation is going one step forward
    connect(animtimer, SIGNAL(timeout()), this, SLOT(stepAnimation()));

    animstep = 0;
}

OGLWidget::~OGLWidget()
{
}


void OGLWidget::stepAnimation()
{
    animstep++;    // Increase animation steps
    update();      // Trigger redraw of scene with paintGL
}

void OGLWidget::initializeGL()
{
    initializeOpenGLFunctions();
          GLfloat lp1[4]  = { 20,  0,  0, 0};
          GLfloat lp2[4]  = { -20, 0 , 0,  0};
          GLfloat red[4]  = {1.0, 1.0 , 0.0,  1};
          GLfloat green[4] = { 0, 1.0, 1.0,  1};
          GLfloat blue[4] = { 0, 0.0, 1.0,  1};
          glClearColor(0,0,0,1);
          glEnable(GL_DEPTH_TEST);
          glDepthFunc(GL_LESS);

          glShadeModel(GL_SMOOTH);
          glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);
          glEnable(GL_LIGHTING);

          glLightfv(GL_LIGHT1, GL_POSITION, lp1);
          glLightfv(GL_LIGHT1, GL_DIFFUSE,  red);
          glLightfv(GL_LIGHT1, GL_SPECULAR, red);
          glEnable(GL_LIGHT1);

          glLightfv(GL_LIGHT2, GL_POSITION, lp2);
          glLightfv(GL_LIGHT2, GL_DIFFUSE,  green);
          glLightfv(GL_LIGHT2, GL_SPECULAR, green);
          glEnable(GL_LIGHT2);

//    glClearColor(0,0,0,1);
//    glEnable(GL_DEPTH_TEST);
//    glEnable(GL_LIGHT0);
//    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void OGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glOrtho(-2, 2, -2, 2, -200, 200);
    glPushMatrix();
    double r = animstep;   // rotate one degree with each step
    double n=20; // Anzahl der Breitengrade
    double m=20; // Anzahl der Längengrade
    double dalpha=2.0*M_PI/n;
    double dbeta=M_PI/m/2.0;
    double k=0,j=0;
    double x1,x2,y1,y2,z1,z2=0;
    double x3,x4,y3,y4,z3,z4=0;

    glRotatef(r, 1.0f, 1.0f, 0.0f); // Rotate by r degrees around z axis


    for(j=0;j < fmin(r/m,n) ;j+=1)
    for(k=0;k <fmin(r-j*n,m);k+=1)
    {
          x1=cos(j*dbeta)*cos(k*dalpha);
          y1=sin(j*dbeta);
          z1=-cos(j*dbeta)*sin(k*dalpha);
          x2=cos((j+1)*dbeta)*cos(k*dalpha);
          y2=sin((j+1)*dbeta);
          z2=-cos((j+1)*dbeta)*sin(k*dalpha);
          x3=cos((j+1)*dbeta)*cos((k+1)*dalpha);
          y3=sin((j+1)*dbeta);
          z3=-cos((j+1)*dbeta)*sin((k+1)*dalpha);
          x4=cos(j*dbeta)*cos((k+1)*dalpha);
          y4=sin(j*dbeta);
          z4=-cos(j*dbeta)*sin((k+1)*dalpha);
          glBegin(GL_QUADS);
          glNormal3d(x1,y1,z1);
          glColor3d(1.0,1.0,0.0);
          glVertex3d(x1,y1,z1);
          //glColor3f(0.0,1.0,0.0);
          glVertex3d(x2,y2,z2);
          //glColor3f(0.0,0.0,1.0);
          glVertex3d(x3,y3,z3);
          //glColor3f(1.0,0.0,1.0);
          glVertex3d(x4,y4,z4);
          glEnd();
          glBegin(GL_QUADS);
          glNormal3d(x1,-y1,z1);
          glColor3f(0.0,0.0,1.0);
          glVertex3d(x1,-y1,z1);
          //glColor3f(0.0,1.0,0.0);
          glVertex3d(x4,-y4,z4);
          //glColor3f(0.0,0.0,1.0);
          glVertex3d(x3,-y3,z3);
          //glColor3f(1.0,0.0,1.0);
          glVertex3d(x2,-y2,z2);
          glEnd();
    }
    glPopMatrix();
}

void OGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


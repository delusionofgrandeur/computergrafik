#include "oglwidget.h"
#include <cmath>

OGLWidget::OGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    rotx = 0;
    roty = 0;
    rotz = 0;
    zoom = 100;
}

OGLWidget::~OGLWidget()
{
}


void OGLWidget::setRotX(int newrx)
{
    rotx = newrx;
    update();
}

void OGLWidget::setRotY(int newry)
{
    roty = newry;
    update();
}

void OGLWidget::setRotZ(int newrz)
{
    rotz = newrz;
    update();
}

void OGLWidget::setZoom(int newzoom)
{
    zoom = newzoom;
    update();
}

void OGLWidget::setUnfold(int newunfold)
{
    unfold = newunfold;
    update();
}


void OGLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0,0,0,1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

    // For wireframe replace GL_FILL with GL_LINE
    glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
}

void OGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    // Apply rotation angles
    glRotatef(rotx, 1.0f, 0.0f, 0.0f); // Rotate around x axis
    glRotatef(roty, 0.0f, 1.0f, 0.0f); // Rotate around y axis
    glRotatef(rotz, 0.0f, 0.0f, 1.0f); // Rotate around z axis

    // Apply scaling
    float scale = zoom/100.0;
    glScalef( scale, scale, scale ); // Scale along all axis

    // Calculate the unfolding (x and y will be mapped to the corresponding direction)
    float unfoldx = 0.5-0.707f*cos( M_PI/4.0+unfold/100.0*M_PI );
    float unfoldy = 0.707f*sin( M_PI/4.0+unfold/100.0*M_PI );

    glBegin(GL_TRIANGLES);
        // Upper triangles
        glColor3f( 1.0f, 0.0f, 0.0f );
        glVertex3f(0, unfoldy, -unfoldx);
        glVertex3f(-0.5f, 0.0f, -0.5f);
        glVertex3f(0.5f, 0.0f, -0.5f);

        glColor3f( 1.0f, 1.0f, 0.0f );
        glVertex3f(unfoldx, unfoldy, 0.0f);
        glVertex3f(0.5f, 0.0f, -0.5f);
        glVertex3f(0.5f, 0.0f, 0.5f);

        glColor3f( 0.0f, 1.0f, 0.0f );
        glVertex3f(0.0f, unfoldy, unfoldx);
        glVertex3f(0.5f, 0.0f, 0.5f);
        glVertex3f(-0.5f, 0.0f, 0.5f);

        glColor3f( 0.0f, 1.0f, 1.0f );
        glVertex3f(-unfoldx, unfoldy, 0.0f);
        glVertex3f(-0.5f, 0.0f, 0.5f);
        glVertex3f(-0.5f, 0.0f, -0.5f);

        // Lower triangles
        glColor3f( 0.0f, 0.0f, 1.0f );
        glVertex3f(0.0f, -0.5f, 0.0f);
        glVertex3f(-0.5f, 0.0f, -0.5f);
        glVertex3f(0.5f, 0.0f, -0.5f);

        glColor3f( 1.0f, 0.0f, 1.0f );
        glVertex3f(0.0f, -0.5f, 0.0f);
        glVertex3f(0.5f, 0.0f, -0.5f);
        glVertex3f(0.5f, 0.0f, 0.5f);

        glColor3f( 1.0f, 0.5f, 0.5f );
        glVertex3f(0.0f, -0.5f, 0.0f);
        glVertex3f(0.5f, 0.0f, 0.5f);
        glVertex3f(-0.5f, 0.0f, 0.5f);

        glColor3f( 0.0f, 0.5f, 1.0f );
        glVertex3f(0.0f, -0.5f, 0.0f);
        glVertex3f(-0.5f, 0.0f, 0.5f);
        glVertex3f(-0.5f, 0.0f, -0.5f);

    glEnd();
}

void OGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void OGLWidget::mousePressEvent(QMouseEvent *event)
{
    // Upon mouse pressed, we store the current position...
    lastpos = event->pos();
}

void OGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    // ... and while moving, we calculate the dragging deltas
    // Left button: Rotating around x and y axis
    int dx = (event->buttons() & Qt::LeftButton) ? lastpos.y() - event->y() : 0;
    int dy = lastpos.x() - event->x();
    // Right button: Rotating around z and y axis
    int dz = (event->buttons() & Qt::RightButton) ? lastpos.y() - event->y() : 0;

    // Now let the world know that we want to rotate
    emit changeRotation( dx, dy, dz );

    // Make the current position the starting point for the next dragging step
    lastpos = event->pos();
}

void OGLWidget::keyPressEvent(QKeyEvent *event)
{
    // This is the delta we want to use for rotating
    const int keyDelta = 10;

    switch(event->key())
    {
        // Up/Down: Rotating around x axis
        case Qt::Key_Up:
            emit changeRotation( keyDelta, 0, 0 );
            break;
        case Qt::Key_Down:
            emit changeRotation( -keyDelta, 0, 0 );
            break;

        // Left/Right: Rotating around y axis
        case Qt::Key_Left:
            emit changeRotation( 0, keyDelta, 0 );
            break;
        case Qt::Key_Right:
            emit changeRotation( 0, -keyDelta, 0 );
            break;

        // Pg up/down: Rotating around z axis
        case Qt::Key_PageUp:
            emit changeRotation( 0, 0, keyDelta );
            break;
        case Qt::Key_PageDown:
            emit changeRotation( 0, 0, -keyDelta );
            break;

        // All other will be ignored
        default:
            break;
    }
}

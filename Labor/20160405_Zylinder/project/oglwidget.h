#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <cstdlib>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <cmath>
#include <math.h>

class OGLWidget : public QOpenGLWidget,
                  protected QOpenGLFunctions
{
    Q_OBJECT

public:
    OGLWidget(QWidget *parent = 0);
    ~OGLWidget();

public slots:
    void setParamA( int newa );
    void setParamB( int newb );
    void setParamC( int newb );
    void setParamO( int newo );

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void drawQuad();
    void drawWuerfel();
    void drawCircle(int radius,int segments);
    void randColor();

protected:
    int parama;
    int paramb;
    int paramo;
    int paramc;

};


#endif // OGLWIDGET_H

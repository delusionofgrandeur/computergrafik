#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Connect slider A to change param A in OGLWidget
    connect(ui->sliderA, SIGNAL(valueChanged(int)), ui->glwidget, SLOT(setParamA(int)));

    // Connect slider B to change param B in OGLWidget
    connect(ui->sliderB, SIGNAL(valueChanged(int)), ui->glwidget, SLOT(setParamB(int)));

    // Connect slider C to change param C in OGLWidget
    connect(ui->sliderC, SIGNAL(valueChanged(int)), ui->glwidget, SLOT(setParamC(int)));

    // Connect slider O to change param O in OGLWidget
    connect(ui->openBoxSlider, SIGNAL(valueChanged(int)), ui->glwidget, SLOT(setParamO(int)));

}

MainWindow::~MainWindow()
{
    delete ui;
}

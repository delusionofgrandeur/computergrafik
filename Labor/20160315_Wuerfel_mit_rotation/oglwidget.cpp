#include "oglwidget.h"

OGLWidget::OGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    parama = 0;
    paramb = 0;
    paramc = 0;
    paramo = 0;
}

OGLWidget::~OGLWidget()
{
}


void OGLWidget::setParamA(int newa)
{
    parama = newa; // Set new value
    update();      // Trigger redraw of scene with paintGL
}

void OGLWidget::setParamB(int newb)
{
    paramb = newb;
    update();
}

void OGLWidget::setParamC(int newc)
{
    paramc = newc;
    update();
}

void OGLWidget::setParamO(int newo)
{
    paramo = newo;
    update();
}

void OGLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    glClearColor(0,0,0,1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

void OGLWidget::paintGL()
{
    double c = parama*3.6; // degree to rotate  X
    double r = paramb*3.6; // degree to rotate  Y
    double z = paramc*3.6; // degree to rotate  Z
    double o = paramo*3.6; // degree to open Box

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glOrtho(-2,2,-2,2,-10,10);
    glRotatef(c, 1.0f, 0.0f, 0.0f);
    glRotatef(r, 0.0f, 1.0f, 0.0f); // Rotate by r degrees around z axis
    glRotatef(z, 0.0f, 0.0f, 1.0f);

    glPushMatrix();
    //glRotated(45,0.0f,1.0f,0.0f);
    glTranslated(-0.5,-0.5,0);

    glColor3d(1,0,0);
    drawQuad();

    glPushMatrix();

    glTranslated(1,0,0);
    glRotated(90,0,1,0);

    glColor3d(0,1,0);
    drawQuad();

    glPushMatrix();

    glTranslated(1,0,0);
    glRotated(90,0,1,0);

    glColor3d(0,0,1);
    drawQuad();

    glPushMatrix();

    glTranslated(1,0,0);
    glRotated(90,0,1,0);

    glColor3d(0,0.5,0.5);
    drawQuad();

    glPushMatrix();

    glRotated(-90,1,0,0);
    glColor3d(0.5,0.5,0);
    drawQuad();

    glPopMatrix();

    glPushMatrix();
    glTranslated(0,1,0);
    glRotatef(-o, 1.0f, 0.0f, 0.0f);
    glColor3d(0.5,0,0.5);
    drawQuad();

    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
}

void OGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void OGLWidget::drawQuad()
{
    glBegin(GL_QUADS);
            glVertex3f(0,0,0);
            glVertex3f(0,1,0);
            glVertex3f(1,1,0);
            glVertex3f(1,0,0);
    glEnd();
}

void OGLWidget::drawWuerfel()
{

}


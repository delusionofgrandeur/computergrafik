#ifndef BALL_H
#define BALL_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <math.h>
#include <iostream>


class Ball
{

public:
    double radius;
    int segments;

    double pos_x;
    double pos_y;
    double pos_z;

    double speed_x;
    double speed_y;
    double speed_z;

    void draw();

    void animationstep(int time_ms);

    Ball();
    Ball(double rad, int seg);

};


#endif // BALL_H

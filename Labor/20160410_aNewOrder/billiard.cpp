#include "billiard.h"

float deskXmin = -7;
float deskXmax = 7;
float deskZmin = -5;
float deskZmax = 5;
float deskYmin = 0;
float deskYmax = 1;

float ballRadius = 0.5;
int ballSegments = 180;

void drawDesk(){

    glColor3d(0,1,0);

    //Boden
    glBegin(GL_QUADS);
        glVertex3d(deskXmin,deskYmin,deskZmin);
        glVertex3d(deskXmin,deskYmin,deskZmax);
        glVertex3d(deskXmax,deskYmin,deskZmax);
        glVertex3d(deskXmax,deskYmin,deskZmin);
    glEnd();


    glColor3d(0,0,1);
    //Seitenteile
    glBegin(GL_TRIANGLE_STRIP);
        glVertex3d(deskXmin,deskYmin,deskZmin);
        glVertex3d(deskXmin,deskYmax,deskZmin);

        glVertex3d(deskXmin,deskYmin,deskZmax);
        glVertex3d(deskXmin,deskYmax,deskZmax);

        glVertex3d(deskXmax,deskYmin,deskZmax);
        glVertex3d(deskXmax,deskYmax,deskZmax);

        glVertex3d(deskXmax,deskYmin,deskZmin);
        glVertex3d(deskXmax,deskYmax,deskZmin);

        glVertex3d(deskXmin,deskYmin,deskZmin);
        glVertex3d(deskXmin,deskYmax,deskZmin);
    glEnd();
}

void drawBilliardBall(float x, float y, float z){
    glPushMatrix();

    glTranslated(x,y+ballRadius,z);

    drawKugel(ballRadius,ballSegments);

    glPopMatrix();
}



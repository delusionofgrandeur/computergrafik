#ifndef BILLIARD_H
#define BILLIARD_H


#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include "geometry.h"

void drawDesk();
void drawBilliardBall(float x,float y,float z);


#endif // BILLIARD_H


#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <cstdlib>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <cmath>
#include <math.h>
#include "geometry.h"
#include "billiard.h"
#include <QTimer>
#include "ball.h"

class OGLWidget : public QOpenGLWidget,
                  protected QOpenGLFunctions
{
    Q_OBJECT

public:
    OGLWidget(QWidget *parent = 0);
    ~OGLWidget();

public slots:
    void setParamA( int newa );
    void setParamB( int newb );
    void setParamC( int newb );
    void stepAnimation();

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();
    void drawQuad();
    void drawWuerfel();
    void randColor();

    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

protected:
    int anim_step_time;

    int parama;
    int paramb;
    int paramc;

    float ball_pos_x;
    float ball_pos_z;
    float ball_speed_x;
    float ball_speed_z;

    QPoint mouse_pressed_point;
    QPoint mouse_released_point;

    QTimer* animtimer;

    Ball ball1;

    void collisionHandling();

};


#endif // OGLWIDGET_H

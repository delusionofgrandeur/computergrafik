#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cstdlib>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <cmath>
#include <math.h>
#include <iostream>

void drawCylinder(double radius,double height, int segments);
void drawWuerfel();
void drawQuad();
void drawKugel(double radius, int segments);
void drawTorus(double radiusOut, double radiusIn, int segments );

void changeColorStatic(int i);
void changeColorToRandom();

#endif // GEOMETRY_H

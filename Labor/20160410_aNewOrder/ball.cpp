#include "ball.h"

Ball::Ball()
{
    radius = 1;
    segments = 24;
    std::cout << "const 1 used";
}

Ball::Ball(double rad , int seg)
{
    radius = rad;
    segments = seg;
    std::cout << "const 2 used";
}

void Ball::draw()
{
    glPushMatrix();
    glTranslated(pos_x,pos_y+radius,pos_z);


    double winkelSegment = 2*M_PI/segments;
    for (int i = 0;i<segments;i++){
        glBegin(GL_TRIANGLE_STRIP);
            for(int j=0;j<=segments;j++){
                double topRadius = radius*sin(winkelSegment*i/2);
                double botRadius = radius*sin(winkelSegment*(i+1)/2);
                glVertex3f(cos(j*winkelSegment)*topRadius,radius*cos(winkelSegment*i/2),-sin(j*winkelSegment)*topRadius);
                glVertex3f(cos(j*winkelSegment)*botRadius,radius*cos(winkelSegment*(i+1)/2),-sin(j*winkelSegment)*botRadius);
            }
        glEnd();
    }

    glPopMatrix();

}

void Ball::animationstep(int time_ms)
{
    pos_x = pos_x + speed_x*(time_ms/1000.0);
    pos_y = pos_y + speed_y*(time_ms/1000.0);
    pos_z = pos_z + speed_z*(time_ms/1000.0);

    //speed_x = speed_x*(0.999);
    //speed_z = speed_z*(0.999);
}

#include "oglwidget.h"

OGLWidget::OGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    anim_step_time = 15;

    parama = 0;
    paramb = 0;
    paramc = 0;

    ball_pos_x = 0;
    ball_pos_z = 0;

    ball_speed_x = 1;
    ball_speed_z = 1;

    ball1 = Ball(0.5,24);
    ball1.pos_x = 0;
    ball1.pos_y = 0;
    ball1.pos_z = 0;
    ball1.speed_x = 5;
    ball1.speed_y = 0;
    ball1.speed_z = 7;

    animtimer = new QTimer(this);
    animtimer->start( anim_step_time );

    // Everytime the timer fires, the animation is going one step forward
    connect(animtimer, SIGNAL(timeout()), this, SLOT(stepAnimation()));

}

void OGLWidget::stepAnimation()
{
//    ball_pos_x += ball_speed_x/(anim_step_time);
//    ball_pos_z += ball_speed_z/(anim_step_time);
    ball1.animationstep(anim_step_time);
    collisionHandling();
    update();
}


OGLWidget::~OGLWidget()
{
}


void OGLWidget::setParamA(int newa)
{
    parama = newa; // Set new value
    update();      // Trigger redraw of scene with paintGL
}

void OGLWidget::setParamB(int newb)
{
    paramb = newb;
    update();
}

void OGLWidget::setParamC(int newc)
{
    paramc = newc;
    update();
}

void OGLWidget::initializeGL()
{
    initializeOpenGLFunctions();

    GLfloat lp1[4] = {20,0,0,0};
    GLfloat lp2[4] = {-20,0,0,0};
    GLfloat lp3[4] = {0,-6,20,0};

    GLfloat red[4] = {1.0,0,0,1};
    GLfloat green[4] = {0,1.0,0,1};
    GLfloat blue[4] = {0,0,1.0,1};

    GLfloat light[4] = {1,1,1,0.8};

    glClearColor(0,0,0,1);
    glShadeModel(GL_SMOOTH);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);

    glLightfv(GL_LIGHT1, GL_POSITION, lp1);
    glLightfv(GL_LIGHT1, GL_DIFFUSE,  light);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light);
    glEnable(GL_LIGHT1);

    glLightfv(GL_LIGHT2, GL_POSITION, lp2);
    glLightfv(GL_LIGHT2, GL_DIFFUSE,  light);
    glLightfv(GL_LIGHT2, GL_SPECULAR, light);
    glEnable(GL_LIGHT2);

    glLightfv(GL_LIGHT3, GL_POSITION, lp3);
    glLightfv(GL_LIGHT3, GL_DIFFUSE,  light);
    glLightfv(GL_LIGHT3, GL_SPECULAR, light);
    glEnable(GL_LIGHT3);

    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);


    GLfloat specular[] = {20.0f, 1.0f, 1.0f , 1.0f};
}

void OGLWidget::paintGL()
{
    double c = parama*3.6; // degree to rotate  X
    double r = paramb*3.6; // degree to rotate  Y
    double z = paramc*3.6; // degree to rotate  Z

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glOrtho(-10,10,-10,10,-100,100);
    glRotatef(c, 1.0f, 0.0f, 0.0f);
    glRotatef(r, 0.0f, 1.0f, 0.0f); // Rotate by r degrees around z axis
    glRotatef(z, 0.0f, 0.0f, 1.0f);

    glColor3d(0,1,1);

    //drawCylinder(1,1,24);
    //drawKugel(1,24);
    //drawTorus(0.5,1,24 );
    drawDesk();
    glColor3d(1,0,0);
    ball1.draw();
    //drawBilliardBall(ball_pos_x,0,ball_pos_z);
}

void OGLWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void OGLWidget::drawQuad()
{
    glBegin(GL_QUADS);
            glVertex3f(0,0,0);
            glVertex3f(0,1,0);
            glVertex3f(1,1,0);
            glVertex3f(1,0,0);
    glEnd();
}

void OGLWidget::randColor(){

    glColor3d(rand()/100,rand()/100,rand()/100);
}

void OGLWidget::mousePressEvent(QMouseEvent *event){

}

void OGLWidget::mouseReleaseEvent(QMouseEvent *event){

}

void OGLWidget::collisionHandling(){
    double desk_x_min = -7;
    double desk_x_max = 7;
    double desk_z_min = -5;
    double desk_z_max = 5;

    if(ball1.pos_x - ball1.radius <= desk_x_min || ball1.pos_x + ball1.radius >= desk_x_max){
        ball1.speed_x = ball1.speed_x*-1;
    }
    if(ball1.pos_z - ball1.radius <= desk_z_min || ball1.pos_z + ball1.radius >= desk_z_max){
        ball1.speed_z = ball1.speed_z*-1;
    }
}

#include "geometry.h"

void drawQuad()
{
    glBegin(GL_QUADS);
            glVertex3f(0,0,0);
            glVertex3f(0,1,0);
            glVertex3f(1,1,0);
            glVertex3f(1,0,0);
    glEnd();
}

void drawWuerfel(){
    glPushMatrix();
    //glRotated(45,0.0f,1.0f,0.0f);
    glTranslated(-0.5,-0.5,0);

    glColor3d(1,0,0);
    drawQuad();

    glPushMatrix();

    glTranslated(1,0,0);
    glRotated(90,0,1,0);

    glColor3d(0,1,0);
    drawQuad();

    glPushMatrix();

    glTranslated(1,0,0);
    glRotated(90,0,1,0);

    glColor3d(0,0,1);
    drawQuad();

    glPushMatrix();

    glTranslated(1,0,0);
    glRotated(90,0,1,0);

    glColor3d(0,0.5,0.5);
    drawQuad();

    glPushMatrix();

    glRotated(-90,1,0,0);
    glColor3d(0.5,0.5,0);
    drawQuad();

    glPopMatrix();

    glPushMatrix();
    glTranslated(0,1,0);
    //glRotatef(-o, 1.0f, 0.0f, 0.0f);
    glColor3d(0.5,0,0.5);
    drawQuad();

    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
    glPopMatrix();
}

void drawCylinder(double radius, double height, int segments)
{
    glPushMatrix();

    float angle = 360 / segments;
    double winkelsegment = 2*M_PI/segments;

    for(int i = 0;i<segments;i++){

        changeColorStatic(i);

        glPushMatrix();
        glBegin(GL_TRIANGLES);
            glVertex3f(0,0,0);
            glVertex3f(radius,0,0);
            glVertex3f(cos(winkelsegment)*radius,0,-sin(winkelsegment)*radius);
        glEnd();

        glBegin(GL_TRIANGLES);
            glVertex3f(0,height,0);
            glVertex3f(radius,height,0);
            glVertex3f(cos(winkelsegment)*radius,height,-sin(winkelsegment)*radius);
        glEnd();

        glBegin(GL_QUADS);
            glVertex3f(radius,0,0);
            glVertex3f(cos(winkelsegment)*radius,0,-sin(winkelsegment)*radius);
            glVertex3f(cos(winkelsegment)*radius,height,-sin(winkelsegment)*radius);
            glVertex3f(radius,height,0);

        glEnd();

        glRotated(angle,0,1,0);



    }




    glPopMatrix();
}

void drawKugel(double radius, int segments){
    glColor3f(1,0,0);
    double winkelSegment = 2*M_PI/segments;
    for (int i = 0;i<segments;i++){

        changeColorStatic(i);

        glBegin(GL_TRIANGLE_STRIP);
            for(int j=0;j<=segments;j++){
                double topRadius = radius*sin(winkelSegment*i/2);
                double botRadius = radius*sin(winkelSegment*(i+1)/2);
                glVertex3f(cos(j*winkelSegment)*topRadius,radius*cos(winkelSegment*i/2),-sin(j*winkelSegment)*topRadius);
                glVertex3f(cos(j*winkelSegment)*botRadius,radius*cos(winkelSegment*(i+1)/2),-sin(j*winkelSegment)*botRadius);
            }
        glEnd();
    }
}

void drawTorus(double radiusOut, double radiusIn, int segments ){

    float angle = 360 / segments;
    double winkelSegment = 2*M_PI/segments;

    for ( int i = 0; i<segments;i++){
        changeColorStatic(i);
        glBegin(GL_TRIANGLE_STRIP);
            for(int j=0;j<=24;j++){
                double a1 = winkelSegment*j;
                double a2 = winkelSegment*(j+1);
                double b1 = winkelSegment*i;
                double b2 = winkelSegment*(i+1);
                //glVertex3f(radiusIn*cos(b1)+cos(a1)*radiusOut*cos(b1)+sin(b1)*radiusOut,radiusOut+sin(a1)*radiusIn,-sin(b1)*radiusOut+cos(b1)*radiusOut);
                //glVertex3f(radiusIn*cos(b2)+cos(a2)*radiusOut*cos(b2)+sin(b2)*radiusOut,radiusOut+sin(a2)*radiusIn,-sin(b2)*radiusOut+cos(b2)*radiusOut);
                glVertex3f(radiusIn*cos(b1)+cos(a1)*radiusOut*cos(b1),sin(a1)*radiusOut,-sin(b1)*radiusIn-sin(b1)*cos(a1)*radiusOut);
                glVertex3f(radiusIn*cos(b2)+cos(a2)*radiusOut*cos(b2),sin(a2)*radiusOut,-sin(b2)*radiusIn-sin(b2)*cos(a2)*radiusOut);

            }
        glEnd();
    }
}


void changeColorStatic(int i){
   int num = i%6;

   if (num == 0){
       glColor3d(1,0,0);
   }else if(num == 1){
       glColor3d(1,1,0);
   }else if(num == 2){
       glColor3d(0,1,0);
   }else if(num == 3){
       glColor3d(0,1,1);
   }else if(num == 4){
       glColor3d(0,0,1);
   }else if(num == 5){
       glColor3d(1,0,1);
   }
}

void changeColorToRandom(){
    int random = rand()%3;
    std::cout << random;
    if (random == 0){
        glColor3d(0,0,1);
    }else if(random == 1){
        glColor3d(0,1,0);
    }else if(random == 2){
        glColor3d(1,0,0);
    }
}
